import axios from '../axios';

/*
 * 地址管理模块， 对应后台的AddressController
 */

/**
 * 查询所有数据
 * @param data 请求参数
 * @returns {Promise | Promise<Address>} 返回所有数据列表
 */
export const findAll = (data) => {
    // 调用后台控制器
    return axios({
        /*
            调用后台请求地址 => 通过此地址可查询到对应的Controller对象。
            /address => 请求的Controller配置的RequestMapping的value.
            /findAll => 请求的Controller配置的
         */
        url: '/address/findAll',
        // http请求的方法: get/post/put/delete
        method: 'get',
        data
    });
};

/**
 *
 * @param data
 * @returns {Promise | Promise<unknown>}
 */
export const findPage = (data) => {
    // 调用后台控制器
    return axios({
        /*
            调用后台请求地址 => 通过此地址可查询到对应的Controller对象。
            /address => 请求的Controller配置的RequestMapping的value.
            /findPage => 请求的Controller配置的
         */
        url: '/address/findPage',
        // http请求的方法: get/post/put/delete
        method: 'post',
        data
    });
};

export const update = (data) => {
    return axios({
       url: '/address/update',
       method: 'post',
       data
    });
}